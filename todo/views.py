import mimetypes

from django.contrib.auth.models import User
from django.shortcuts import HttpResponse
from django_filters import rest_framework as filters
from rest_framework import generics
from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import (
    NotFound,
    MethodNotAllowed,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from web_tokens.authentication import JWTAuthentication
from .filters import TodoListFilter
from .models import Todo, TodoList
from .permissions import IsOwnerOrReadOnly
from .serializers import (
    TodoSerializer,
    TodoUploadImageSerializer,
    UserSerializer,
    ListOfTodoListsSerializer,
)


################################
#                              #
#          TODOS VIEWS         #
#                              #
################################


class TodoViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing todo instances.
    """
    serializer_class = TodoSerializer
    queryset = Todo.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    authentication_classes = (JWTAuthentication,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def _update_or_upload_image(self, request, instance):
        serializer = self.get_serializer(instance, data=request.FILES)
        serializer.is_valid(raise_exception=True)
        serializer.save()

    @action(methods=['get', 'post', 'delete', 'patch'], url_path='image',
            detail=True, serializer_class=TodoUploadImageSerializer)
    def todo_image_view(self, request, pk):
        todo = self.get_object()

        if request.method == 'POST':
            self._update_or_upload_image(request=request, instance=todo)
            return Response(status=status.HTTP_201_CREATED)

        if request.method == 'PATCH':
            self._update_or_upload_image(request=request, instance=todo)
            return Response(status=status.HTTP_200_OK)

        if request.method == 'GET':
            image = todo.image
            if not image:
                raise NotFound
            img_data = todo.image.read()
            img_path = todo.image
            content_type = mimetypes.guess_type(str(img_path))[0]
            response = HttpResponse(img_data, content_type=content_type)
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(img_path.name)
            return response

        if request.method == 'DELETE':
            image = todo.image
            if image:
                todo.image.delete()
                todo.save()
                return Response(status=status.HTTP_204_NO_CONTENT)

            raise NotFound

        raise MethodNotAllowed


class TodosList(generics.ListCreateAPIView):
    """
    APIView that returns list of todos or allows to create a new one
    """
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    authentication_classes = (JWTAuthentication,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    APIView that allows to read and update information about a todo
    """
    # Allows to upload files
    # parser_classes = (FileUploadParser,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    authentication_classes = (JWTAuthentication,)


################################
#                              #
#          TODOLISTS VIEWS     #
#                              #
################################


class ListOfTodoLists(generics.ListCreateAPIView):
    """
    APIView that allows to retrieve a list of todo_lists or create a new one
    """
    serializer_class = ListOfTodoListsSerializer
    permission_classes = (permissions.IsAuthenticated,
                          IsOwnerOrReadOnly,)
    authentication_classes = (JWTAuthentication,)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TodoListFilter

    def get_queryset(self):
        return TodoList.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TodoListDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    APIView that allows to Read, Update and delete a TodoList
    """
    queryset = TodoList.objects.all()
    serializer_class = ListOfTodoListsSerializer
    authentication_classes = (JWTAuthentication,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)


################################
#                              #
#     USER-RELATED VIEWS       #
#                              #
################################


class UserList(generics.ListAPIView):
    """
    Returns list of users
    """
    queryset = User.objects.all()
    authentication_classes = (JWTAuthentication,)
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    """
    Returns detailed user info
    """
    authentication_classes = (JWTAuthentication,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserCreate(APIView):
    """
    Creates the user.
    """
    def post(self, request, format='json'):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                data = serializer.data
                return Response(data=data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
