from django.db import models
from django.contrib.auth.models import User


class TodoList(models.Model):
    title = models.CharField(max_length=512, default="Unnamed list")
    user = models.ForeignKey(
        User, related_name='todo_lists',
        on_delete=models.CASCADE, null=False
    )

    completed = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.title}. " \
               f"{Todo.objects.filter(todo_list=self).count()} notes"


class Todo(models.Model):
    title = models.CharField(max_length=256)
    text = models.TextField(default="")
    created_at = models.DateTimeField(auto_now_add=True)
    to_be_done_date = models.DateTimeField(null=True)
    is_done = models.BooleanField(default=False)
    user = models.ForeignKey(
        User, related_name='todos', on_delete=models.CASCADE, null=False
    )
    todo_list = models.ForeignKey(
        TodoList, related_name='todos', on_delete=models.CASCADE, null=False
    )  # not null
    failed = models.BooleanField(default=False)
    last_notification = models.DateTimeField(default=None, null=True, blank=True)
    image = models.ImageField(null=True, blank=False, upload_to='uploaded-data/todos-images')

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return f"{self.title}"
