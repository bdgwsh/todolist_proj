import datetime

from .models import Todo

from celery import Celery
from celery import shared_task

from django.core.mail import EmailMessage
from django.db.models import Q

app = Celery('todolist_project', backend='amqp',
             broker='amqp://rabbitmq:rabbitmq@rabbitmq:5672/')


@shared_task
def send_email(subject, msg, to=[]):
    email = EmailMessage(
        subject,
        msg,
        to=to,
        headers={'Reply-To': None}
    )

    email.send()


@shared_task
def notify_users_with_outdated_todos():
    now = datetime.datetime.now(datetime.timezone.utc)
    outdated_todos = Todo.objects.select_related('user').filter(
        Q(to_be_done_date__isnull=False, failed=False) &
        Q(to_be_done_date__lte=datetime.datetime.now()) &
        Q(last_notification__isnull=True) | Q(last_notification__lt=now),
    )
    for todo in outdated_todos:
        subject = f"You failed todo: {todo.title}"
        msg = f"It seems that you failed a todo. \n" \
              f"Todo: {todo.title} \n" \
              f"Expiration date: {todo.to_be_done_date}\n" \
              f"Text: {todo.text}"
        send_email.delay(subject, msg, to=[todo.user.email])
    outdated_todos.update(failed=True)


@shared_task
def notify_before_hour():
    notification_info = []
    now = datetime.datetime.now(datetime.timezone.utc)
    expiration_date = now + datetime.timedelta(hours=1)
    todos = Todo.objects.select_related('user').filter(
        Q(to_be_done_date__isnull=False, failed=False) &
        Q(last_notification__isnull=True) &
        Q(to_be_done_date__lte=expiration_date, to_be_done_date__gte=now)
    )

    for todo in todos:
        subject = f"1 Hour left before your todo: {todo.title}"
        msg = f"It seems that you have 1 hour left for doing your todo. \n" \
              f"Todo: {todo.title} \n" \
              f"Has to be done at: {str(todo.to_be_done_date)} \n" \
              f"Text: {todo.text}"
        notification_info.append({
            'to': [todo.user.email],
            'subject': subject,
            'msg': msg
        })
    for notification in notification_info:
        send_email.delay(**notification)
    todos.update(last_notification=now)
