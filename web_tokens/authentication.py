
from .utils import get_token_payload, validate_payload

from django.utils.translation import ugettext as _
from django.contrib.auth.models import User


from rest_framework import authentication
from rest_framework import exceptions


class JWTAuthentication(authentication.BaseAuthentication):
    """
    JWT AUTHORIZATION
    """
    def authenticate(self, request):
        access_token = request.META.get('HTTP_AUTHORIZATION')
        if not access_token:
            return None
        access_token = access_token.strip()
        payload = get_token_payload(access_token)
        if not validate_payload(payload=payload):
            return None

        user_id = payload.get('user_id', None)
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            msg = _('No such user')
            raise exceptions.AuthenticationFailed(msg)
        return (user, None)
