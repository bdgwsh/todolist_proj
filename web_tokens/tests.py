import datetime

from todo.models import Todo, TodoList

from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from freezegun import freeze_time


class TestJWT(APITestCase):

    def setUp(self):
        self.username = "tester"
        self.password = "testing"

        self.urls = {
            'token_auth': 'web_tokens:obtain-token-pair',
            'token_refresh': 'web_tokens:refresh-token',
        }

        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
        )

        self.test_todo_list_obj = TodoList.objects.create(title='Testing Title',  user=self.user)
        self.test_todo_obj = Todo.objects.create(
            title='Testing Title',
            text='Testing text',
            user=self.user,
            todo_list=self.test_todo_list_obj
        )

        self.testing_data = {
            'title': 'Test',
            'text': 'Testing',
            'todo_list': self.test_todo_list_obj.id
        }

    def tearDown(self):
        pass

    # Test protected endpoint without auth
    def test_protected_unauth(self):
        data = {'title': 'Test', 'text': 'text'}
        url = reverse('todo:todo-viewset-list')
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    # Test protected endpoint with auth
    def test_protected_auth(self):
        token_pair = self.get_token_pair()
        access_token = token_pair.get('access_token')
        auth = '{0}'.format(access_token)
        url = reverse('todo:todo-viewset-list')
        response = self.client.post(url, data=self.testing_data,  HTTP_AUTHORIZATION=auth, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, [response.data, access_token])

    # Test token-refresh endpoint with expired refresh_token)
    def test_expired_refresh_token(self):
        token_pair = self.get_token_pair()
        refresh_token = token_pair.get('refresh_token')
        with freeze_time(datetime.datetime.now() + datetime.timedelta(days=2)):
            data = {'refresh_token': refresh_token}
            url = reverse(self.urls.get('token_refresh'))
            response = self.client.post(url, data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    # Test forbidden endpoints with expired access token
    def test_expired_access_token(self):
        token_pair = self.get_token_pair()
        access_token = token_pair.get('access_token')
        with freeze_time(datetime.datetime.now() + datetime.timedelta(days=2)):
            auth = '{0}'.format(access_token)
            url = reverse('todo:todo-viewset-list')
            response = self.client.post(url, data=self.testing_data, HTTP_AUTHORIZATION=auth, format='json')
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, [response.data, token_pair])

    # Refreshing expired access token
    def test_refreshing_expired_access_token(self):
        token_pair = self.get_token_pair()
        access_token = token_pair.get('access_token')
        refresh_token = token_pair.get('refresh_token')

        with freeze_time(datetime.datetime.now() + datetime.timedelta(seconds=1000)):
            auth = '{0}'.format(access_token)
            url = reverse('todo:todo-viewset-list')
            response = self.client.post(url, data=self.testing_data, HTTP_AUTHORIZATION=auth, format='json')
            self.assertEqual(
                response.status_code,
                status.HTTP_403_FORBIDDEN,
                [response.data, access_token]
            )
            data = {"refresh_token": refresh_token}
            url = reverse(self.urls.get('token_refresh'))
            response = self.client.post(url, data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
            refreshed_token = response.data.get('access_token', None)
            self.assertIsNotNone(refreshed_token)
            url = reverse('todo:todo-viewset-list')
            auth = '{0}'.format(refreshed_token)
            response = self.client.post(
                url,
                data=self.testing_data,
                HTTP_AUTHORIZATION=auth, format='json')
            self.assertEqual(
                response.status_code,
                status.HTTP_201_CREATED,
                [response.data, refresh_token]
            )

    # Test token-refresh endpoint
    def test_token_refresh(self):
        token_pair = self.get_token_pair()
        data = {"refresh_token": token_pair.get('refresh_token')}
        url = reverse(self.urls.get('token_refresh'))
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertIsNotNone(response.data.get('access_token'))
        self.assertIsNotNone(response.data.get('refresh_token'))

    # Get valid JWT token
    def get_token_pair(self):
        auth = {"username": self.username, "password": self.password}
        url = reverse(self.urls.get('token_auth'))
        response = self.client.post(url, data=auth)
        self.assertIsNotNone(response.data.get('access_token'))
        self.assertIsNotNone(response.data.get('refresh_token'))
        # Refresher.objects.create(user=self.user, refresh_token=response.data['refresh_token'])
        token_pair = response.data
        return token_pair
